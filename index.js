const conventionalRecommendedBump = require(`conventional-recommended-bump`);
const semver = require('semver')
const commandLineArgs = require('command-line-args')
const { XMLParser, XMLBuilder, XMLValidator} = require("fast-xml-parser");
const fs=require("fs")
const path=require('path')
const _=require('lodash')
const simpleGit = require('simple-git');
const format = require("string-template")

const optionDefinitions = [
    { name: 'commit', alias: 'c', type: String },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'preRelease', type: String },
    { name: 'file', type: String, multiple: false},
    { name: 'field', type: String,alias: 'f', multiple: false},
   
  ]
const options = commandLineArgs(optionDefinitions)
conventionalRecommendedBump({
  preset: `angular`
}, (crbError, recommendation) => {
    if (crbError) {
        console.error(crbError)
        return
      }
    const executionPath=process.cwd()
    const filePath=path.join(executionPath,options.file)
    fs.readFile(filePath, 'utf8' , (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError)
          return
        }
       
         const parser = new XMLParser({ignoreAttributes : false});
         const builder = new XMLBuilder({format:true, ignoreAttributes : false});
         const jObj = parser.parse(data);
         let candidateVersion=""
         const releaseType=_.get(jObj,options.field);
       if(options.preRelease){
        candidateVersion=semver.inc(releaseType,'prerelease',options.preRelease)
       }else{
         candidateVersion=semver.inc(releaseType,recommendation.releaseType)
       }
       _.set(jObj,options.field,candidateVersion);
       let samleXmlData = builder.build(jObj);
       
       fs.writeFile(filePath, samleXmlData, (fsWriteError) => {
        if (fsWriteError)
          console.error(fsWriteError);
        else {
         
          const git = simpleGit();
      if(options.commit){
        git.add('./*')
        git.commit(format(options.commit,{version:candidateVersion}))
      }
        }
      });
       
        console.log(candidateVersion)
      })
   
});