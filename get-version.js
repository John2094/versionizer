#!/usr/bin/env node
const conventionalRecommendedBump = require(`conventional-recommended-bump`);
const semver = require('semver')
const commandLineArgs = require('command-line-args')
const { XMLParser, XMLBuilder, XMLValidator} = require("fast-xml-parser");
const fs=require("fs")
const path=require('path')
const _=require('lodash')
const simpleGit = require('simple-git');
const format = require("string-template")

const optionDefinitions = [
    
    { name: 'verbose', alias: 'v', type: Boolean },
    
    { name: 'file', type: String, multiple: false},
    { name: 'field', type: String,alias: 'f', multiple: false},
   
  ]
const options = commandLineArgs(optionDefinitions)
conventionalRecommendedBump({
  preset: `angular`
}, (crbError, recommendation) => {
    if (crbError) {
        console.error(crbError)
        return
      }
    const executionPath=process.cwd()
    const filePath=path.join(executionPath,options.file)
    fs.readFile(filePath, 'utf8' , (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError)
          return
        }
       
         const parser = new XMLParser({ignoreAttributes : false});
         const jObj = parser.parse(data);
         let candidateVersion=""
         const releaseType=_.get(jObj,options.field);
       if(options.preRelease){
        candidateVersion=semver.inc(releaseType,'prerelease',options.preRelease)
       }else{
         candidateVersion=semver.inc(releaseType,recommendation.releaseType)
       }
     
       
        console.log(candidateVersion)
      })
   
});