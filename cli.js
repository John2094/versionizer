#!/usr/bin/env node
const conventionalRecommendedBump = require(`conventional-recommended-bump`);
const semver = require('semver');
const commandLineArgs = require('command-line-args');
const { XMLParser, XMLBuilder, XMLValidator } = require('fast-xml-parser');
const fs = require('fs');
const path = require('path');
const _ = require('lodash');
const simpleGit = require('simple-git');
const format = require('string-template');
const { exec } = require("child_process");

const mainDefinitions = [{ name: 'command', defaultOption: true }];
const mainOptions = commandLineArgs(mainDefinitions,{ stopAtFirstUnknown: true });
const argv = mainOptions._unknown || [];


if (mainOptions.command === 'set' || mainOptions.command === 'set-release') {
  const optionDefinitions = [
    { name: 'json', alias: 'j', type: Boolean },
    { name: 'push', alias: 'p', type: Boolean,multiple: false },
    { name: 'commit', alias: 'c', type: String },
    { name: 'remote', alias: 'r', type: String },
    { name: 'tag', alias: 't', type: String },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'preRelease', type: String },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  if(options.verbose){
    console.log("Call set version")
  }
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      if(options.verbose){
        console.log("search for file version in ",filePath);
      }
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }
        if(options.verbose){
          console.log("file found")
        }
        let jObj = undefined
        if(options.json){
          jObj =JSON.parse(data)
        }else{
          const parser = new XMLParser({ ignoreAttributes: false });
        
        const jObj = parser.parse(data);
        }
       
        let candidateVersion = '';
        const version = _.get(jObj, options.field);
        if (options.preRelease) {
        if(mainOptions.command === 'set-release'){
          candidateVersion = semver.coerce(
            version,
            'prerelease',
            options.preRelease
          ).version;
        }else if(mainOptions.command === 'set'){
          candidateVersion = semver.inc(
            version,
            'prerelease',
            options.preRelease
          );
        }
         
        } else {
          if(mainOptions.command === 'set-release'){
            candidateVersion = semver.coerce(
              version,
              recommendation.releaseType
            ).version;
          }else if(mainOptions.command === 'set'){
            candidateVersion = semver.inc(
              version,
              recommendation.releaseType
            );
          }
         
        }
        _.set(jObj, options.field, candidateVersion);
        let writeData = undefined;
        if(options.json){
          writeData  = JSON.stringify(jObj,null,2)
        }else{
          const builder = new XMLBuilder({
            format: true,
            ignoreAttributes: false,
          });
          writeData = builder.build(jObj);
        }
       

        fs.writeFile(filePath, writeData, (fsWriteError) => {
          if (fsWriteError) console.error(fsWriteError);
          else {
           
            if (options.commit) {
              const git = simpleGit();
              git.add('./*',(eAdd,sAdd)=>{
               
                if(options.verbose){
                  if(eAdd){
                    console.error("git add",eAdd)
                  }
                  console.log("git add",sAdd)
                  git.commit(format(options.commit, { version: candidateVersion }),(eCommit,sCommit)=>{
                    if(options.verbose){
                      if(eCommit){
                        console.error("git commit",eCommit)
                      }
                      console.log("git commit",sCommit)
                    }
                    if (options.tag) {
                      git.addTag(format(options.tag, { version: candidateVersion }),(eTag,sTag)=>{
                        if(options.verbose){
                          if(eTag){
                            console.error("git tag",eTag)
                          }
                          console.log("git tag",sTag)
                          git.status((eStatus,sStatus)=>{
                            if(options.verbose){
                              if(eStatus){
                                console.error("git commit",eStatus)
                              }
                              console.log("git commit",sStatus)
                            }
                            const origin=options.remote?options.remote:'origin';
                            
                           if(options.push){
                            if(options.verbose){
                              console.log("Pushing to origin",origin)
                            }
                            git.push(origin,sStatus.current,(ePush,sPush)=>{
                              if(options.verbose){
                                if(ePush){
                                  console.error("git push",ePush)
                                }
                                console.log("git push",sPush)
                              }
                            })
                           }
                          });
                        }
                      });
                    }else{
                      git.status((eStatus,sStatus)=>{
                        if(options.verbose){
                          if(eStatus){
                            console.error("git commit",eStatus)
                          }
                          console.log("git commit",sStatus)
                        }
                        const origin=options.remote?options.remote:'origin';
                      
                        if(options.push){
                          if(options.verbose){
                            console.log("Pushing to origin",origin)
                          }
                          git.push(origin,sStatus.current,(ePush,sPush)=>{
                            if(options.verbose){
                              if(ePush){
                                console.error("git push",ePush)
                              }
                              console.log("git push",sPush)
                            }
                          })
                         }
                      });
                    }
                   
                  });
                }
              });
             
              
            }
          }
        });

        console.log(candidateVersion);
      });
    }
  );
}
if (mainOptions.command === 'change-prerelease' ) {
  const optionDefinitions = [
    { name: 'json', alias: 'j', type: Boolean },
    { name: 'push', alias: 'p', type: Boolean,multiple: false },
    { name: 'commit', alias: 'c', type: String },
    { name: 'remote', alias: 'r', type: String },
    { name: 'tag', alias: 't', type: String },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'preRelease', type: String },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  if(options.verbose){
    console.log("Call set version")
  }
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      if(options.verbose){
        console.log("search for file version in ",filePath);
      }
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }
        if(options.verbose){
          console.log("file found")
        }
        let jObj = undefined
        if(options.json){
          jObj =JSON.parse(data)
        }else{
          const parser = new XMLParser({ ignoreAttributes: false });
        
        const jObj = parser.parse(data);
        }
       
        let candidateVersion = '';
        const version = _.get(jObj, options.field);
        const coerced=semver.coerce(
          version,
          'prerelease',
          options.preRelease
        ).version;
        const prereleased=semver.prerelease(
          semver.inc(
            version,
            'prerelease',
            options.preRelease
          )
        )
      
        candidateVersion = `${coerced}-${ prereleased.join(".")}` 
       
       
         
        
        _.set(jObj, options.field, candidateVersion);
        let writeData = undefined;
        if(options.json){
          writeData  = JSON.stringify(jObj,null,2)
        }else{
          const builder = new XMLBuilder({
            format: true,
            ignoreAttributes: false,
          });
          writeData = builder.build(jObj);
        }
       

        fs.writeFile(filePath, writeData, (fsWriteError) => {
          if (fsWriteError) console.error(fsWriteError);
          else {
           
            if (options.commit) {
              const git = simpleGit();
              git.add('./*',(eAdd,sAdd)=>{
               
                if(options.verbose){
                  if(eAdd){
                    console.error("git add",eAdd)
                  }
                  console.log("git add",sAdd)
                  git.commit(format(options.commit, { version: candidateVersion }),(eCommit,sCommit)=>{
                    if(options.verbose){
                      if(eCommit){
                        console.error("git commit",eCommit)
                      }
                      console.log("git commit",sCommit)
                    }
                    if (options.tag) {
                      git.addTag(format(options.tag, { version: candidateVersion }),(eTag,sTag)=>{
                        if(options.verbose){
                          if(eTag){
                            console.error("git tag",eTag)
                          }
                          console.log("git tag",sTag)
                          git.status((eStatus,sStatus)=>{
                            if(options.verbose){
                              if(eStatus){
                                console.error("git commit",eStatus)
                              }
                              console.log("git commit",sStatus)
                            }
                            const origin=options.remote?options.remote:'origin';
                            
                           if(options.push){
                            if(options.verbose){
                              console.log("Pushing to origin",origin)
                            }
                            git.push(origin,sStatus.current,(ePush,sPush)=>{
                              if(options.verbose){
                                if(ePush){
                                  console.error("git push",ePush)
                                }
                                console.log("git push",sPush)
                              }
                            })
                           }
                          });
                        }
                      });
                    }else{
                      git.status((eStatus,sStatus)=>{
                        if(options.verbose){
                          if(eStatus){
                            console.error("git commit",eStatus)
                          }
                          console.log("git commit",sStatus)
                        }
                        const origin=options.remote?options.remote:'origin';
                      
                        if(options.push){
                          if(options.verbose){
                            console.log("Pushing to origin",origin)
                          }
                          git.push(origin,sStatus.current,(ePush,sPush)=>{
                            if(options.verbose){
                              if(ePush){
                                console.error("git push",ePush)
                              }
                              console.log("git push",sPush)
                            }
                          })
                         }
                      });
                    }
                   
                  });
                }
              });
             
              
            }
          }
        });

        console.log(candidateVersion);
      });
    }
  );
}
 else if (mainOptions.command === 'get') {
  const optionDefinitions = [
    { name: 'json', alias: 'e', type: Boolean },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }

        const parser = new XMLParser({ ignoreAttributes: false });
        let jObj =undefined;
        if(options.json){
          jObj=JSON.parse(data)
        }else{
          jObj=parser.parse(data);
        }
        let candidateVersion = '';
        const version = _.get(jObj, options.field);
      

        console.log(version);
      });
    }
  );
}else if (mainOptions.command === 'get-next') {
  const optionDefinitions = [
    { name: 'json', alias: 'e', type: Boolean },
    { name: 'preRelease', type: String },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }

        const parser = new XMLParser({ ignoreAttributes: false });
        let jObj =undefined;
        if(options.json){
          jObj=JSON.parse(data)
        }else{
          jObj=parser.parse(data);
        }
        
        let candidateVersion = '';
        const version = _.get(jObj, options.field);
        if (options.preRelease) {
          candidateVersion = semver.inc(
            version,
            'prerelease',
            options.preRelease
          );
        } else {
          candidateVersion = semver.inc(
            version,
            recommendation.releaseType
          );
        }

        console.log(candidateVersion);
      });
    }
  );
}else if (mainOptions.command === 'get-release') {
  const optionDefinitions = [
    { name: 'json', alias: 'e', type: Boolean },
    { name: 'verbose', alias: 'v', type: Boolean },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }

        const parser = new XMLParser({ ignoreAttributes: false });
        let jObj =undefined;
        if(options.json){
          jObj=JSON.parse(data)
        }else{
          jObj=parser.parse(data);
        }
        
        let candidateVersion = '';
        const version = _.get(jObj, options.field);
        if (options.preRelease) {
          candidateVersion = semver.coerce(
            version,
            'prerelease',
            options.preRelease
          ).version;
        } else {
          candidateVersion = semver.coerce(
            version,
            recommendation.releaseType
          ).version;
        }

        console.log(candidateVersion);
      });
    }
  );
}
else if (mainOptions.command === 'isPrerelease') {
  const optionDefinitions = [
    { name: 'json', alias: 'e', type: Boolean },
    { name: 'file', type: String, multiple: false },
    { name: 'field', type: String, alias: 'f', multiple: false },
    { name: 'preid', alias: 'p', type: String },
   
  ];
  const options = commandLineArgs(optionDefinitions, { argv });
  conventionalRecommendedBump(
    {
      preset: `angular`,
    },
    (crbError, recommendation) => {
      if (crbError) {
        console.error(crbError);
        return;
      }
      const executionPath = process.cwd();
      const filePath = path.join(executionPath, options.file);
      fs.readFile(filePath, 'utf8', (fsReadError, data) => {
        if (fsReadError) {
          console.error(fsReadError);
          return;
        }

        const parser = new XMLParser({ ignoreAttributes: false });
        let jObj =undefined;
        if(options.json){
          jObj=JSON.parse(data)
        }else{
          jObj=parser.parse(data);
        }
        
        let candidateVersion = '';
        
        const version = _.get(jObj, options.field);
        const pr=semver.prerelease(version);
      
        if (options.preid) {
         
         return console.log(pr && pr.length>0 && pr[0]==options.preid?"true":"false")
        } else {
          return  console.log(pr && pr.length>0?"true":"false")
        }

      });
    }
  );
}